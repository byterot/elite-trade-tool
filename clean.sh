#!/bin/sh -e

if [ ! -d bin ] || [ ! -d build ] || [ ! -d src ] || [ ! -f clean.sh ]; then 
	echo 'NOT in root directory. clean cancelled';
	exit 1
fi


rm -r bin/
rm -r build/
echo 'project cleaned'
