#!/bin/sh -e

CMAKE_OUT=ett
DATA_OUT=$1

#---------
#- build -
#---------

mkdir -p build
mkdir -p bin

cd build
if [ -z "$2" ]; then 
  BUILD_TYPE=3 #production build
  #BUILD_TYPE=0 #debug build
else
  BUILD_TYPE=$2
fi

if echo "$(uname -a)" | grep -q "Linux"
then
  SYSTEM="linux"
elif echo "$(uname -a)" | grep -q "Darwin"
then
  SYSTEM="mac"
elif echo "$(uname -a)" | grep -q "MINGW32_NT-5" || echo "$(uname -a)" | grep -q "MINGW32_NT-6"
then
  SYSTEM="win-xp"
  DATA_OUT="ett"
else
  SYSTEM="windows"
  #SYSTEM="win-xp" # temporary fix for dll problems
  DATA_OUT="ett"
fi

if [ "$SYSTEM" = "windows" ]; then
  echo "using windows toolchain"
  cmake .. -G "Unix Makefiles" -DCMAKE_TOOLCHAIN_FILE="toolchains/win32.cmake" -DBUILD_TYPE=$BUILD_TYPE
elif [ "$SYSTEM" = "win-xp" ]; then
  echo "using windows-xp toolchain"
  cmake .. -G "Unix Makefiles" -DCMAKE_TOOLCHAIN_FILE="toolchains/win32xp.cmake" -DBUILD_TYPE=$BUILD_TYPE
elif [ "$SYSTEM" = "mac" ]; then
  echo "using mac toolchain"
  cmake .. -G "Unix Makefiles" -DCMAKE_TOOLCHAIN_FILE="toolchains/mac.cmake" -DBUILD_TYPE=$BUILD_TYPE
else
  cmake .. -DBUILD_TYPE=$BUILD_TYPE
fi

if [ "$SYSTEM" = "mac" ]; then
  NUM_WORKERS=4 # most macs have at least 4 threads these days
else
  NUM_WORKERS=$(grep -P '^processor\t: [0-9]+$' /proc/cpuinfo | wc -l)
fi
if [ "$BUILD_TYPE" -eq 2 ]; then
  NUM_WORKERS=1
fi

echo "-- Starting make with $NUM_WORKERS workers"

if [ "$SYSTEM" = "windows" ] || [ "$SYSTEM" = "win-xp" ]; then
  mingw32-make -j$NUM_WORKERS
else
  make -j$NUM_WORKERS 
fi

cd ..

rm bin/* || true

if [ "$BUILD_TYPE" -eq 0 ]; then
  cp build/$CMAKE_OUT bin/$1
else
  upx -1 -o bin/$1 build/$CMAKE_OUT
fi
