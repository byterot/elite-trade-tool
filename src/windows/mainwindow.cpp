#include "windows/mainwindow.h"
#include "helpers.h"

MainWindow::MainWindow(std::function<void(std::function<void()>)> d) : dispatch(d),
  load_thread([this]() 
  {
      elitedata = std::make_unique<EliteData>([this](int progress, std::string task)
      {
        dispatch([this, progress, task]()
            {
                load_text->setText(QString::fromStdString(task));
                load_bar->setValue(progress);
            });
      });
  })
{
  layout = std::make_unique<QGridLayout>();  
  source_system = std::make_unique<QLineEdit>();
  test_button = std::make_unique<QPushButton>("Test");
  
  load_bar = std::make_unique<QProgressBar>();
  load_bar->setMaximum(100);

  load_text = std::make_unique<QLabel>();
  load_text->setText(QString::fromStdString("Hey"));
  

  layout->addWidget(load_text.get(), 0, 0, 1, 1);
  layout->addWidget(load_bar.get(), 0, 1, 1, 1);
  layout->addWidget(source_system.get(), 1, 0, 1, 1);
  layout->addWidget(test_button.get(), 1, 1, 1, 1);

  window = std::make_unique<QWidget>();
  window->setLayout(layout.get());
  window->setWindowTitle("Elite Trade Tool");
  //setCentralWidget(window.get());
  window->show();

  load_thread.detach();
}
