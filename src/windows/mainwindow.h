#pragma once

#include <QMainWindow>
#include <QGridLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QProgressBar>
#include <QLabel>
#include <QString>

#include <memory>
#include <functional>
#include <thread>

#include "data/elitedata.h"

class MainWindow 
{
private:
  std::unique_ptr<QWidget> window;
  std::unique_ptr<QGridLayout> layout;
  std::unique_ptr<QLineEdit> source_system;
  std::unique_ptr<QPushButton> test_button;
  std::unique_ptr<QProgressBar> load_bar;
  std::unique_ptr<QLabel> load_text;

  std::function<void(std::function<void()>)> dispatch;
  std::unique_ptr<EliteData> elitedata;
  std::thread load_thread;

public:
  MainWindow(std::function<void(std::function<void()>)> d);
};
