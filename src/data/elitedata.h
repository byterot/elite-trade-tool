#pragma once

#include <cstdint>
#include <iostream>
#include <memory>
#include <nlohmann/json.hpp>
#include <vector>
#include <map>
#include <algorithm>
#include "helpers.h"
#include <math.h>
#include <functional>

using json = nlohmann::json;

struct Listing {
	uint64_t id,
					 station_id,
					 commodity_id,
					 supply,
					 demand,
					 collected_at;

	int64_t buy_price,
					 sell_price;


	Listing() :
		id(0),
		station_id(0),
		commodity_id(0),
		supply(0),
		buy_price(0),
		sell_price(0),
		demand(0),
		collected_at(0) {}

	Listing(uint64_t i,
			uint64_t station,
			uint64_t commodity,
			uint64_t supp,
			int64_t buy,
			int64_t sell,
			uint64_t dem,
			uint64_t collected) : 
		id(i),
		station_id(station),
		commodity_id(commodity),
		supply(supp),
		buy_price(buy),
		sell_price(sell),
		demand(dem),
		collected_at(collected) {}

	void print() {
		std::cout << sell_price << ", " << buy_price << ", " << demand << ", " << supply << std::endl;
	}
};

struct System {
	uint64_t id;
	std::string name;
	float x,y,z;

	float distance(const System& o) {
		float deltaX = x - o.x;
		float deltaY = y - o.y;
		float deltaZ = z - o.z;

		return sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ);
	}
};

struct Station {
	uint64_t id;
	uint64_t system_id;
	std::string name;
	uint64_t distance_to_star;
	uint8_t max_landing = 0; // 0 = small, 1 = medium, 2 = large
	bool planetary;
};

struct Commodity {
	uint64_t id;
	std::string name;
};

struct Trade {
	Station from, to;
	Listing from_listing, to_listing;
	Commodity commodity;
	System from_system, to_system;

	std::string display() {
		std::stringstream ss;

		ss << "Buy " << commodity.name << " at " << from.name << " in " << from_system.name << "\n"; 
		ss << "Fly to " << to_system.name << "\nDock at " << to.name << "\n";
		ss << "Buy price: " << from_listing.buy_price << " Sell price: " << to_listing.sell_price << "\n";
		ss << "Profit per ton: " << (to_listing.sell_price - from_listing.buy_price) << "\n";

		return ss.str();
	}

	int profit() const {
		return to_listing.sell_price - from_listing.buy_price;
	}
};

class EliteData {
	private:
		std::vector<Station> stations;
		std::vector<System> systems;
		std::vector<Commodity> commodities;
		std::map<uint64_t, uint64_t> listing_lookup;

		bool download_file(const std::string& url, const char* file);
		void load_systems();
		void build_listing_lookup();
		void build_stations_cache();
	public:
    std::function<void(int, std::string)> load_callback;

		EliteData(std::function<void(int, std::string)> callback);
		~EliteData();


		void recache();

		std::vector<Listing> get_listings(std::vector<uint64_t>& station_ids);
		std::vector<Station> get_stations(const std::vector<System>& systems, uint8_t min_landing = 1, bool planetary = false);
		bool get_station(const std::string& name, Station& s);
		bool get_station(uint64_t id, Station& s);

		std::vector<System> get_systems_near(const System& system, float max_jump_dist);
		bool get_system(const std::string& name, System& s);
		bool get_system(const Station& station, System& s);

		bool get_commodity(uint64_t id, Commodity& c);
		std::vector<Trade> get_profitable_trades(const Station& origin, const std::vector<Station>& stations);
		std::vector<Trade> get_fuzzy_profitable_trades(const std::vector<Station>& stations);
};
