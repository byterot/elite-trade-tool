#include "elitedata.h"
#include <curl/curl.h>
#include <stdio.h>
#include <string>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <fstream>
#include <queue>
#include <unistd.h>
#include <thread>

const char* commodities_file = "./commodities.csv";
const char* systems_file = "./systems.json";
const char* stations_file = "./stations.json";
const char* listings_file = "./listings.csv";
const	std::string base_url = "https://eddb.io/archive/v6/";

EliteData::EliteData(std::function<void(int, std::string)> callback) : load_callback(callback) {
  recache();
}

EliteData::~EliteData() {

}

void EliteData::recache() {
	std::cout << "Downloading commodities file..." << std::endl;
	download_file(base_url + "commodities.json", commodities_file);

	std::cout << "Downloading systems file..." << std::endl;
	download_file(base_url + "systems_populated.json", systems_file);
	std::thread systems_loader(&EliteData::load_systems, this);

	std::cout << "Downloading stations file..." << std::endl;
	download_file(base_url + "stations.json", stations_file);
	std::thread stations_loader(&EliteData::build_stations_cache, this);

	std::cout << "Downloading listings file..." << std::endl;
	download_file(base_url + "listings.csv", listings_file);
  std::thread listings_loader(&EliteData::build_listing_lookup, this);

	stations.clear();
	systems.clear();
	commodities.clear();
  listing_lookup.clear();

	std::cout << "loading commodities json" << std::endl;
	std::ifstream ic(commodities_file);
	json commodities_json;
	ic >> commodities_json;
	ic.close();

	for (json::iterator it = commodities_json.begin(); it != commodities_json.end(); ++it) {
		Commodity c;
		c.id = (*it)["id"];
		c.name = (*it)["name"];

		commodities.push_back(c);
	}

	
	systems_loader.join();
	listings_loader.join();
	stations_loader.join();
}

void EliteData::load_systems() {
	std::cout << "loading systems json" << std::endl;

	std::ifstream i(systems_file);
	json systems_json;
	i >> systems_json;
	i.close();

	for (json::iterator it = systems_json.begin(); it != systems_json.end(); ++it) {
		System s;	
		s.id = 	(*it)["id"];
		s.name =(*it)["name"];
		s.x = 	(*it)["x"];
		s.y = 	(*it)["y"];
		s.z = 	(*it)["z"];

		systems.push_back(s);
	}

	std::cout << "systems json loaded" << std::endl;
}

size_t write_data(void *ptr, size_t size, size_t nmemb, FILE *stream) {
    size_t written = fwrite(ptr, size, nmemb, stream);
    return written;
}

static int progress_func(void* clientp, double dltotal, double dlnow, double ultotal, double ulnow)
{
  EliteData* data = static_cast<EliteData*>(clientp);
  std::string task("Downloading elite data files");
  data->load_callback((int)(dlnow / dltotal * 100.0), task);

  return (int)0;
}

bool EliteData::download_file(const std::string& url, const char* file) {
	CURL* curl;
	FILE* fp;
	CURLcode res;

	curl = curl_easy_init();
	if(curl) {
		fp = fopen(file, "wb");
		curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
    curl_easy_setopt(curl, CURLOPT_NOPROGRESS, false);
    curl_easy_setopt(curl, CURLOPT_PROGRESSDATA, this);
    curl_easy_setopt(curl, CURLOPT_PROGRESSFUNCTION, progress_func);
    //curl_easy_setopt(curl, CURLOPT_XFERINFOFUNCTION, progress_func);
		res = curl_easy_perform(curl);
		/* always cleanup */
		curl_easy_cleanup(curl);
		fclose(fp);

		return true;
	}

	return false;
}

void EliteData::build_listing_lookup() {
	FILE* fp;
	char buf[128];
	size_t len;
	ssize_t read;
	std::vector<Listing> ret;
	std::queue<char> queue;
	std::vector<std::string> segs;

	std::cout << "Building listing lookup" << std::endl;

	fp = fopen(listings_file, "rb");
	fseek(fp, 0, SEEK_END);
	uint64_t size = ftell(fp), size_step = size / 100, next_size_display = size_step;
	fseek(fp, 0, SEEK_SET);
	//getline(&line, &len, fp);
	read = fscanf(fp, "%[^\n]", &buf);
	fseek(fp, 1, SEEK_CUR);

	uint64_t pos = ftell(fp),
					 lastpos = ftell(fp);
	uint64_t id = 0;

	while((read = fscanf(fp, "%[^\n]", &buf)) > 0) {
		fseek(fp, 1, SEEK_CUR); //skip newline
		segs.clear();
		str_split(buf, segs, ',');
	  Listing	l(
				stoull(segs[0]),
				stoull(segs[1]),
				stoull(segs[2]),
				stoull(segs[3]),
				stoll(segs[5]),
				stoll(segs[6]),
				stoull(segs[7]),
				stoull(segs[9]));

		if(id == 0) // id 0 does not exist
			id = l.station_id; 

		if(id != l.station_id) {
			listing_lookup[id] = pos;
			pos = lastpos;
			id = l.station_id;
		}

		lastpos = ftell(fp);
		if(lastpos >= next_size_display) {
			next_size_display += size_step;
			int percent_complete = lastpos / (size / 100);
			std::cout << "building: " << percent_complete << "%" << std::endl;
      if(load_callback)
      {
        load_callback(percent_complete, "parsing data");
      }
		}
	}
/*
	while((read = getline(&line, &len, fp)) != -1) {
	}

	listing_lookup[id] = pos;
*/

	fclose(fp);
	std::cout << "Listing lookup built" << std::endl;
}

void EliteData::build_stations_cache() {
	stations.clear();
	std::cout << "Building stations cache" << std::endl;

	auto x = [&](int depth, json::parse_event_t event, json& parsed) {
		if (event == json::parse_event_t::object_end)
		{
			if(depth == 1)
			{

				if(!parsed["has_market"] || !parsed["has_docking"]) return false;
				if(parsed["id"] == nullptr) return false;
				if(parsed["system_id"] == nullptr) return false;
				if(parsed["distance_to_star"] == nullptr) return false;
				if(parsed["max_landing_pad_size"] == nullptr) return false;
				if(parsed["name"] == nullptr) return false;

				Station s = { 
						(parsed["id"]),
						(parsed["system_id"]),
						(parsed["name"]),
						(parsed["distance_to_star"])
				};

				if(parsed["max_landing_pad_size"] == "L")
					s.max_landing = 2;
				else if(parsed["max_landing_pad_size"] == "M")
					s.max_landing = 1;
				else s.max_landing = 0;

				s.planetary = parsed["is_planetary"];

				stations.push_back(s);
			}
			return false; // do not store the object value
		}
		else return true;
	};

	std::ifstream f(stations_file);
	(void)json::parse(f, x);

	f.close();
	std::cout << "Stations cache built" << std::endl;
}


std::vector<Listing> EliteData::get_listings(std::vector<uint64_t>& station_ids) {
	FILE* fp;
	char buf[128];
	size_t len;
	ssize_t read;
	std::vector<Listing> ret;

	std::sort(station_ids.begin(), station_ids.end());

	fp = fopen(listings_file, "rb");
	std::vector<std::string> segs;
	
	for(auto i: station_ids) {
		fseek(fp, listing_lookup[i], SEEK_SET);

		while((read = fscanf(fp, "%[^\n]", &buf)) > 0) {
			fseek(fp, 1, SEEK_CUR); //skip newline
			segs.clear();
			str_split(buf, segs, ',');
			Listing l(
					stoull(segs[0]),
					stoull(segs[1]),
					stoull(segs[2]),
					stoull(segs[3]),
					stoll(segs[5]),
					stoll(segs[6]),
					stoull(segs[7]),
					stoull(segs[9]));

			if(l.station_id == i)
				ret.push_back(l);
			else break;
		}
	}

	fclose(fp);

	return ret;
}

std::vector<Station> EliteData::get_stations(const std::vector<System>& systems, uint8_t min_landing, bool planetary) {
	std::vector<Station> ret;

	for(auto& s: stations) {
		if(!planetary && s.planetary) continue;
		for(auto& sys: systems) {
			if(s.system_id == sys.id && min_landing <= s.max_landing ) {
				ret.push_back(s);
			}
		}
	}
	return ret;
}

std::vector<System> EliteData::get_systems_near(const System& system, float max_jump_dist) {
	std::vector<System> ret;
	
	for(auto& it: systems)
		if(it.distance(system) <= max_jump_dist)
			ret.push_back(it);

	return ret;
}

bool EliteData::get_station(const std::string& name, Station& s) {
	for(auto& station: stations) 
		if(equals_ignore_case(station.name, name)) {
			s = station;
			return true;
		}
	return false;
}

bool EliteData::get_station(uint64_t id, Station& s) {
	for(auto& station: stations) 
		if(station.id == id) {
			s = station;
			return true;
		}
	return false;
}

bool EliteData::get_system(const std::string& name, System& s) {
	for(auto& it: systems) {
		if(equals_ignore_case(it.name, name)) {
			s = it;
			return true;
		}
	}
	return false;
}
bool EliteData::get_system(const Station& station, System& s) {
	for(auto& it: systems) {
		if(it.id == station.system_id) {
			s = it;
			return true;
		}
	}

	return false;
}

bool EliteData::get_commodity(uint64_t id, Commodity& c) {
	for(auto& commodity: commodities) {
		if(commodity.id == id) {
			c = commodity;
			return true;
		}
	}

	return false;
}

std::vector<Trade> EliteData::get_profitable_trades(const Station& origin, const std::vector<Station>& stations) {
	std::vector<Trade> ret;

	std::vector<uint64_t> station_ids = {origin.id};
	auto origin_listings = get_listings(station_ids);

	station_ids.clear();
	for(auto& s: stations) 
		station_ids.push_back(s.id);

	auto listings = get_listings(station_ids);

	for(auto& origin_listing: origin_listings) {
		if(origin_listing.supply <= 0) continue;

		for(auto& listing: listings) {
			if(listing.demand <= 0) continue;

			if(listing.commodity_id != origin_listing.commodity_id) continue;

			Station listing_station;
			System listing_system, system;
			Commodity commodity;

			get_station(listing.station_id, listing_station);
			get_commodity(origin_listing.commodity_id, commodity);
			get_system(listing_station, listing_system);
			get_system(origin, system);

			for(auto& s: stations)
				if(s.id == listing.station_id) {
					listing_station = s;
					break;
				}

			ret.push_back(Trade {
					origin,
					listing_station,
					origin_listing,
					listing,
					commodity,
					system,
					listing_system
					});
		}
	}

	return ret;
}

std::vector<Trade> EliteData::get_fuzzy_profitable_trades(const std::vector<Station>& stations) {
	std::vector<Trade> ret;

	std::vector<uint64_t> station_ids;
	for(auto& s: stations) 
		station_ids.push_back(s.id);

	auto listings = get_listings(station_ids);

	for(auto& buy_listing: listings) {
		if(buy_listing.supply <= 0) continue;

		for(auto& sell_listing: listings) {
			if(sell_listing.demand <= 0) continue;
			if(buy_listing.commodity_id != sell_listing.commodity_id) continue;
			if(buy_listing.id == sell_listing.id) continue;

			Station buy_listing_station, sell_listing_station;
			System buy_listing_system, sell_listing_system;
			Commodity commodity;

			get_station(buy_listing.station_id, buy_listing_station);
			get_station(sell_listing.station_id, sell_listing_station);
			get_system(buy_listing_station, buy_listing_system);
			get_system(sell_listing_station, sell_listing_system);

			get_commodity(buy_listing.commodity_id, commodity);

			ret.push_back(Trade {
					buy_listing_station,
					sell_listing_station,
					buy_listing,
					sell_listing,
					commodity,
					buy_listing_system,
					sell_listing_system
					});
		}
	}

	return ret;
}
