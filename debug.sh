#!/bin/sh -e

./build.sh game 0

if [ -z "$1" ]; then 
  cd bin
  cgdb game
  cd ..
  exit 0
fi

if [ "$1" = "x" ]; then
  cd bin
  cgdb game -ex 'r' game
  cd ..
  exit 0
fi

cd bin
cgdb -ex "b $1" -ex 'r' game
cd ..
