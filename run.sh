#!/bin/sh -e

if echo "$(uname -a)" | grep -q "Linux"
then
	SYSTEM="linux"
  NAME=ett
elif echo "$(uname -a)" | grep -q "Darwin"
then
	SYSTEM="mac"
  NAME=ett
elif echo "$(uname -a)" | grep -q "MINGW32_NT-5"
then
  SYSTEM="win-xp"
  NAME=ett.exe
else
	SYSTEM="windows"
  NAME=ett.exe
fi

./build.sh $NAME $1
cd bin
./$NAME

