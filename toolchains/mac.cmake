# the name of the target operating system
SET(CMAKE_SYSTEM_NAME Darwin)

set(SDL2_INCLUDE_DIR /usr/local/include/)
#set(SDL2_INCLUDE_DIR /usr/local/Cellar/sdl2/2.0.10/include)

# which compilers to use for C and C++
#SET(CMAKE_C_COMPILER gcc)
#SET(CMAKE_CXX_COMPILER g++)
#SET(CMAKE_RC_COMPILER mingw32-windres)
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -L/usr/local/include -F/Library/Frameworks")
#set(SDL2_LIBRARY "/usr/local/Cellar/sdl2/")
#set(SDL2_INCLUDE_DIR "/usr/local/include/SDL2")


# adjust the default behaviour of the FIND_XXX() commands:
# search headers and libraries in the target environment, search
# programs in the host environment
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
