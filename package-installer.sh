#!/bin/bash

if [[ $(uname -a) =~ "Linux" ]]; then
	SYSTEM="linux"
elif [[ $(uname -a) =~ "Darwin" ]]; then
	SYSTEM="mac"
else
	SYSTEM="windows"
fi

if [ "$SYSTEM" = "linux" ]; then
	if [ -f "/etc/arch-release" ]; then
		pacman -S cmake upx cgdb
	else
		apt install cmake upx-ucl cgdb gcc-multilib g++-multilib qt5-default libcurl4-dev libcurl4-openssl-dev
	fi
elif [ "$SYSTEM" = "mac" ]; then
	/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

	brew install cmake upx cgdb gdb
else
	pacman -S --disable-download-timeout vim mingw-w64-i686-cmake make git cmake gcc cgdb upx mingw-w64-x86_64-qt5  mingw-w64-i686-qt-creator mingw-w64-x86_64-qt-creator
fi
